<?php

namespace App\Repository;

use App\Entity\WordKey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WordKey|null find($id, $lockMode = null, $lockVersion = null)
 * @method WordKey|null findOneBy(array $criteria, array $orderBy = null)
 * @method WordKey[]    findAll()
 * @method WordKey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WordKeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WordKey::class);
    }

    // /**
    //  * @return WordKey[] Returns an array of WordKey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WordKey
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
